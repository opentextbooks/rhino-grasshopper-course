# Personalized fit

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3


:::{grid-item-card}
:link: Human_3D_Shape_Data_and_Models/!index
:link-type: doc
:img-top: Human_3D_Shape_Data_and_Models/cover_crop.png
:class-header: bg-light

Human 3D Shape Data and Models
:::

:::{grid-item-card}
:link: Data-Driven_Personalized_Design/!index
:link-type: doc
:img-top: Data-Driven_Personalized_Design/cover_crop.png
:class-header: bg-light

Data-Driven Personalized Design
:::







