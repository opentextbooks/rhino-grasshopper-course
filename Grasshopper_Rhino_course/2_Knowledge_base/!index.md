# Knowledge Base

The knowledge base for the Rhino Grasshopper course serves as a comprehensive resource, providing detailed tutorials on applied topics in industrial design. As a dynamic platform, it's designed to grow, with plans to continually expand by incorporating more information, insights, and resources in the future to enhance the learning and exploration of computational design at Industrial Design Engineering.

## [Personalized Fit](/Grasshopper_Rhino_course/2_Knowledge_base/Personalized_fit/!index.md)

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Personalized_fit/Human_3D_Shape_Data_and_Models/!index
:link-type: doc
:img-top: Personalized_fit/Human_3D_Shape_Data_and_Models/cover_crop.png
:class-header: bg-light

Human 3D Shape Data and Models
:::

:::{grid-item-card}
:link: Personalized_fit/Data-Driven_Personalized_Design/!index
:link-type: doc
:img-top: Personalized_fit/Data-Driven_Personalized_Design/cover_crop.png
:class-header: bg-light

Data-Driven Personalized Design
:::
:::::

## [Digital Fabrication](/Grasshopper_Rhino_course/2_Knowledge_base/Digital_fabrication/!index.md)

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Digital_fabrication/Laser_cutting/!index
:link-type: doc
:img-top: Digital_fabrication/Laser_cutting/cover_crop.png
:class-header: bg-light

Generating Laser Cutting Files with Grasshopper
:::

:::{grid-item-card}
:link: Digital_fabrication/3D_Printing/!index
:link-type: doc
:img-top: Digital_fabrication/3D_Printing/cover_crop.png
:class-header: bg-light

Generating 3D Printing Files (G-code) with Grasshopper
:::
:::::

## [Generative Design](/Grasshopper_Rhino_course/2_Knowledge_base/Generative_Design/!index.md)

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Generative_Design/Generative_Form_Finding/!index
:link-type: doc
:img-top: Generative_Design/Generative_Form_Finding/cover_crop.png
:class-header: bg-light

Generative Form Finding with Kangaroo Physics (plugin)
:::
:::::










