# External resources

[TU Delft Architecture Grasshopper Course](http://wiki.bk.tudelft.nl/toi-pedia/Grasshopper)

[Rhino Tutorial Page](https://www.rhino3d.com/learn/?query=kind:%20jump_start&modal=null)

[Medium: intro to Grasshopper](https://medium.com/intro-to-grasshopper)

[Cademy Webinars](https://www.cademy.xyz/webinars)

[Parametric by Design](https://parametricbydesign.com/)