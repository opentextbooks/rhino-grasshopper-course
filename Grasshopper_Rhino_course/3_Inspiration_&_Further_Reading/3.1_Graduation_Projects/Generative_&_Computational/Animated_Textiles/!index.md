# Exploring Animated Textiles Using Pneumatic Actuators

```{tags} 3D-Printing, Simulation
```

<div class="card">
    <p><strong>Authors:</strong> J. Martínez Castro<br>
    <strong>Supervisors:</strong> E. Karana (mentor), J. Wu (graduation committee), A. Buso (graduation committee)<br>
    <strong>Graduation date:</strong> 1 October 2021<br>
    <strong>MSc thesis:</strong> Find it <a href="http://resolver.tudelft.nl/uuid:de1fe683-02fd-4482-a554-4fbe5d2d70a2"> here</a>
    </p>
</div>

```{figure} texalive_overview.webp
[Textalive prototype](http://resolver.tudelft.nl/uuid:de1fe683-02fd-4482-a554-4fbe5d2d70a2) ©️ 2021 by J. Martínez Castro. Reproduced with permission.
```

```{figure} texalive_grasshopper.png
[Textalive prototype grasshopper interface](http://resolver.tudelft.nl/uuid:de1fe683-02fd-4482-a554-4fbe5d2d70a2) ©️ 2021 by J. Martínez Castro. Reproduced with permission.
```

## Abstract

This project explores how 3D printed pneumatic soft actuators can be used to enhance the expressiveness of textiles with alive-like movement. The research begins by studying current literature on shape changing materials and interfaces in order to select the best candidate material to explore during the project. The project then uses a material driven approach to characterize the 3D printed pneumatic textiles for enhancing their performance and ease of fabrication. Based on the characterization, a material concept is created to showcase the material qualities found during the research and to help study the material experience of shape changing interfaces in future research.

As a result of the research, we introduce Textalive: The Animated Textile Toolkit, a fully 3D printed approach to explore shape changing interfaces and alive-like expressions including its hardware augmented by the computational tool. The toolkit uses accessible 3D printed pneumatic actuators commonly used in soft robotics due to their controllability and ease of fabrication compared to other shape changing materials. The 3D printed pneumatic actuators can be arranged along a 3D printed textile composite to create a variety of shape morphologies. The hardware allows the user to control the kinetic parameters of the movement of the textile to create different expressions. Additionally, the computational design tool allows the designer to predict the shape and movement of the textile by digitally varying the location of the actuator. The toolkit was validated with various designers ranging from different levels of expertise with smart materials showing its potential as a design tool for easily exploring shape changing interfaces and alive-like expressions.

*Abstract from [MSc graduation thesis](http://resolver.tudelft.nl/uuid:de1fe683-02fd-4482-a554-4fbe5d2d70a2) ©️ 2021 by J. Martínez Castro. Reproduced with permission.*

<div class="card">
    <p><strong>Page last Edited:</strong> 16/02/2024<br>
    <strong>Edited by:</strong> Dieter van Dortmont, Willemijn Elkhuizen<br>
    </p>
</div>