# Generative Data-driven Design & Computational Simulation

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Animated_Textiles/!index
:link-type: doc
:img-top: Animated_Textiles/cover_crop.png
:class-header: bg-light

Pneumatically animated textiles
:::

:::{grid-item-card}
:link: Smart_Pillow/!index
:link-type: doc
:img-top: Smart_Pillow/cover_crop.png
:class-header: bg-light

Smart Pillow
:::
