# Instrumentation - Capturing Anthropometric Data

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Low-cost_3D_Foot_Scanner/!index
:link-type: doc
:img-top: Low-cost_3D_Foot_Scanner/cover_crop.jpg
:class-header: bg-light

Low cost 3D Foot Scanner


:::
