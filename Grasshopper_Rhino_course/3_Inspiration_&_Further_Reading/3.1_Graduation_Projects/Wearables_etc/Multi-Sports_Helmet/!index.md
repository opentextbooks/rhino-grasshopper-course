# Designing a multi-sports helmet based on 3D ergonomic data

```{tags} 3d-scans, personalized
```

<div class="card">
    <p><strong>Authors:</strong> S.W.H. van Seggelen<br>
    <strong>Supervisors:</strong> J.F.M. Molenbroek (mentor), A. de Smit (mentor), L. Dekker (mentor), C. Heusschen (mentor)<br>
    <strong>Graduation date:</strong> 30 September 2015<br>
    <strong>MSc thesis:</strong> Find it <a href="http://resolver.tudelft.nl/uuid:4eb016fa-9e4a-4fc5-a128-99f762cfbff3"> here</a>
    </p>
</div>

```{figure} 3d_print_helmet.png
[Multi-sports helmet](http://resolver.tudelft.nl/uuid:4eb016fa-9e4a-4fc5-a128-99f762cfbff3) ©️ 2015 by S.W.H. van Seggelen. Reproduced with permission.
```

## **Abstract**

For a Dutch helmet manufacturer, a product proposal has been made. The key feature of the product, is the development done based on 3D scanning techniques. The actual project was twofold; The scientific part of the project was about developing an algorithm for incorporating 3D scanning data in product design. The main challenge was to automatically align the scans based in the Grasshopper environment. After this, an envelope based on a certain percentile in 3D space could be retrieved which is direct geometry input for the actual design, without the need of traditional measurements. Subdivision modeling was used to have full control over the thickness of the different layers of the product. The other part of the project contains the analysis of the company and industry, in an internal and external analysis. Via this way, the strengths, weaknesses, opportunities and threats are being discovered. This analysis output has been the input for the synthesis part; A classical product development which consisted of sketching, modeling and prototyping of the actual helmet.

*Abstract from [MSc graduation thesis](http://resolver.tudelft.nl/uuid:4eb016fa-9e4a-4fc5-a128-99f762cfbff3) ©️ 2015 by S.W.H. van Seggelen. Reproduced with permission.*

<div class="card">
    <p><strong>Page last Edited:</strong> 16/02/2024<br>
    <strong>Edited by:</strong> Dieter van Dortmont, Willemijn Elkhuizen<br>
    </p>
</div>