# A custom fit parametric bra based on 3D body scanning technologies

```{tags} 3d-scans, personalized
```

<div class="card">
    <p><strong>Authors:</strong> L.V.A. van Twillert<br>
    <strong>Supervisors:</strong> J.F.M. Molenbroek (mentor), L. Goto (mentor)<br>
    <strong>Graduation date:</strong> 11 June 2015<br>
    <strong>MSc thesis:</strong> Find it <a href="http://resolver.tudelft.nl/uuid:d7528f9a-1c24-45d9-bbac-d9dff4e080b9"> here</a>
    </p>
</div>

```{figure} parametric_bra.jpg
[Custom fit parametric bra](http://resolver.tudelft.nl/uuid:d7528f9a-1c24-45d9-bbac-d9dff4e080b9) ©️ 2015 by L.V.A. van Twillert. Reproduced with permission.
```

## **Abstract**

3D body scanning and 3D printing are promising techniques to innovate the fashion industry by making custom fit garments more feasible. Especially in the area of tight fit garments, for example lingerie, these techniques are applicable. This thesis describes the development of a custom fit bra using 3D body scanning technologies. While using 3D body scans as input, an attempt was made to automate the production of a custom fit bra. Most women wear a bra every day. Though, virtually all these women are not pleased with this piece of clothing. A bra has to give support to the breasts, feel comfortable and when wearing, women want to look good. In conventional bras, the two dimensional underwire that shapes and supports the breast is forced to follow the three dimensional body. This causes irritations and can leave permanent marks on the body. There are bras available that are more comfortable, though these bras are considered to be only functional, not aesthetically pleasing. It is hard for women to find a bra that is both comfortable, supportive and aesthetically pleasing. Next to this dilemma, finding the perfect bra is difficult because the current bra system is complex and inadequate. It is based on just two body measurements, while much more measurements are required for a perfect fit. Furthermore, every woman is asymmetrical, still the current bra sizing system does not take into account this asymmetry. Over the last decades, the bra has not changed much. Tailor made bras are expensive and still rely on old fashioned techniques. With new technologies like 3D body scanning and 3D printing, custom fit bras should be much more feasible and will take away the struggles women have with their bras. It is chosen for this project to limit the research to women with average breast sizes. By doing so, the outcome of this project forms a solid base that can be scaled down for women with smaller or scaled up for women with larger breast sizes. An attempt was made to come up with an innovative bra that will fulfil a woman’s need. The focus of the project is on the construction of a parametric breast support element. This element is created on a personal body scan using Rhino and Grasshopper. Several prototypes of the breast supportive elements were made and tested to proof this new approach on breast support is better than current solutions. The final result of this research consists of two complete custom fit bras with 3D printed breast support elements. One of the bras was tested by an actress on stage, the other bra was tested during a normal work day. The results were promising as the participants both preferred the newly invented bra type.

*Abstract from [MSc graduation thesis](http://resolver.tudelft.nl/uuid:d7528f9a-1c24-45d9-bbac-d9dff4e080b9) ©️ 2015 by L.V.A. van Twillert. Reproduced with permission.*

<div class="card">
    <p><strong>Page last Edited:</strong> 16/02/2024<br>
    <strong>Edited by:</strong> Dieter van Dortmont, Willemijn Elkhuizen<br>
    </p>
</div>