# Design of customizable sunglasses based on additive manufacturing techniques

```{tags} 3d-scans, personalized
```

<div class="card">
    <p><strong>Authors:</strong> R. van Wijngaarden<br>
    <strong>Supervisors:</strong> Y. Song (mentor), A. de Smit (Mentor)<br>
    <strong>Graduation date:</strong> 9 May 2014<br>
    <strong>MSc thesis:</strong> Find it <a href="http://resolver.tudelft.nl/uuid:33a65390-41bb-4fb2-b2ac-7166fca7e87a"> here</a>
    </p>
</div>

```{figure} cover_crop.jpeg
[Parametric sunglasses](http://resolver.tudelft.nl/uuid:33a65390-41bb-4fb2-b2ac-7166fca7e87a) ©️ 2014 by R. van Wijngaarden. Reproduced with permission.
```

## Abstract

The advent of additive manufacturing techniques offers innovative solutions to customizable consumer products. This paper presents the design and development of customizable sunglasses using additive manufacturing processes. By leveraging the flexibility and scalability of technologies such as 3D printing, we introduce a novel approach to sunglasses design that allows for individual customization in terms of size, shape, color, and additional features. The paper begins with an exploration of the current challenges in the eyewear industry, such as mass production limitations, lack of personalization, and environmental concerns. Subsequently, we introduce our design methodology, utilizing computer-aided design (CAD) software and additive manufacturing techniques to create sunglasses tailored to individual preferences and needs.

Several iterations of prototypes were fabricated, testing various materials and design principles. The process is optimized for efficiency, aesthetics, durability, and comfort, while minimizing waste and reducing the carbon footprint. A comparative analysis with traditional manufacturing methods is also provided, demonstrating the superiority of additive manufacturing in terms of customization capabilities and sustainability. Additionally, we address potential commercial applications and the future scalability of the process.

In conclusion, this study provides a comprehensive overview of the potential additive manufacturing holds for the eyewear industry, particularly in creating unique, environmentally-friendly, and customizable sunglasses. The findings of this study have implications for both the eyewear industry and the broader field of consumer product design, revealing the untapped potential of 3D printing in creating personalized, on-demand products.

*Abstract from [MSc graduation thesis](http://resolver.tudelft.nl/uuid:33a65390-41bb-4fb2-b2ac-7166fca7e87a) ©️ 2014 by R. van Wijngaarden. Reproduced with permission.*

<div class="card">
    <p><strong>Page last Edited:</strong> 16/02/2024<br>
    <strong>Edited by:</strong> Dieter van Dortmont, Willemijn Elkhuizen<br>
    </p>
</div>