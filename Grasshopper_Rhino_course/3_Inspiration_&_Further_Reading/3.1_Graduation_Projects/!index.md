# Graduation Projects

## [Instrumentation - Capturing Anthropometric Data](/Grasshopper_Rhino_course/3_Inspiration_&_Further_Reading/3.1_Graduation_Projects/Instrumentation/!index.md)

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Instrumentation/Low-cost_3D_Foot_Scanner/!index
:link-type: doc
:img-top: Instrumentation/Low-cost_3D_Foot_Scanner/cover_crop.jpg
:class-header: bg-light

Low-cost 3D Foot Scanner
:::

:::::

## [Wearables, Body Protection, and Fashion](/Grasshopper_Rhino_course/3_Inspiration_&_Further_Reading/3.1_Graduation_Projects/Wearables_etc/!index.md)

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Wearables_etc/Sunglasses/!index
:link-type: doc
:img-top: Wearables_etc/Sunglasses/cover_crop.jpeg
:class-header: bg-light

Parametric Sunglasses
:::

:::{grid-item-card}
:link: Wearables_etc/Multi-Sports_Helmet/!index
:link-type: doc
:img-top: Wearables_etc/Multi-Sports_Helmet/cover_crop.png
:class-header: bg-light

Multi-Sports Helmet
:::

:::{grid-item-card}
:link: Wearables_etc/Bra/!index
:link-type: doc
:img-top: Wearables_etc/Bra/cover_crop.jpg
:class-header: bg-light

Custom fit bra
:::

:::::

## [Orthoses and Prostheses](/Grasshopper_Rhino_course/3_Inspiration_&_Further_Reading/3.1_Graduation_Projects/Orthoses_and_Prostheses/!index.md)

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Orthoses_and_Prostheses/Medical_Hand_Splint/!index
:link-type: doc
:img-top: Orthoses_and_Prostheses/Medical_Hand_Splint/cover_crop.jpeg
:class-header: bg-light

Medical Hand Splint
:::

:::{grid-item-card}
:link: Orthoses_and_Prostheses/Prosthetic_Socket/!index
:link-type: doc
:img-top: Orthoses_and_Prostheses/Prosthetic_Socket/cover_crop.jpeg
:class-header: bg-light

Prosthetic Socket
:::

:::{grid-item-card}
:link: Orthoses_and_Prostheses/Ankle_Foot_Orthosis/!index
:link-type: doc
:img-top: Orthoses_and_Prostheses/Ankle_Foot_Orthosis/cover_crop.png
:class-header: bg-light

Ankle Foot Orthosis
:::

:::::