# A medical jewel, the hand splint

```{tags} 3d-scans, personalized
```

<div class="card">
    <p><strong>Authors:</strong> L. Keijser<br>
    <strong>Supervisors:</strong> T. Huysmans (mentor), A.L.M. Minnoye (mentor)<br>
    <strong>Graduation date:</strong> 24 January 2022<br>
    <strong>MSc thesis:</strong> Find it <a href="http://resolver.tudelft.nl/uuid:8689a874-2460-4a80-b39a-70010d6562c9"> here</a>
    </p>
</div>

```{figure} medical_jewel.jpeg
[A medical jewel: the personalised hand splint](http://resolver.tudelft.nl/uuid:8689a874-2460-4a80-b39a-70010d6562c9) ©️ 2022 by L. Keijser. Reprinted with permission.
```

## Abstract

Your hands are the most precious tools in daily life. Therefore, it is easy to imagine that people with diminished hand functionality experience a reduced quality of life. This implies for patients with joint problems. Consequently, the hand functionality should be increased through splinting. Splinting is a non-operative treatment for patients with commonly rheumatoid arthritis, osteoarthritis and ehlers danlos syndrome.In this master thesis, an ultra personalized splint and service system has been designed. This product-service system includes the whole patient journey, from medical consult to gathering anthropometric data and producing the personalized splint. Throughout the design process, a computational design approach was leading. This started with defining the patient needs and the splint functionality. Thereafter, several options for data and parameter acquisition were explored. The options differed in the complexity of anthropometric data. The first concept was based on a 3D scan (3D), the second on a photograph (2D), and the third on hand measurements (1D). The latter involved the use of a statistical shape model (SSM).With regard to the 3D scanning procedure, both mesh wrapping and digital posture correction were implemented and evaluated. Unfortunately, this did not result in an accurate design reference. Further research on posture correction for orthosis design is therefore recommended.Additional research on 3D scanning of patients with rheumatoid arthritis gave great insight into the possibilities concerning the 3D scanning process. 3D scanning involves great challenges with regard to patients with pathological hands. The process remains not viable until an instant 3D hand scanner becomes available. The SSM is proposed as a bridging solution towards the instant 3D scan or eventually incorporating the hand scan in artificial intelligence (AI) processing workflows.Furthermore, a parametric computational design has been modelled. This design automatically adjusts to the anthropometric data of the patient. It allows for customization of the medical requirements and the aesthetic features. There has been emphasized on medical jewellery design. Different materials have been explored for this purpose.Finally, the fit and functionality evaluation showed good results. The splint fits snugly and only a few aspect with regard to the functionality needs to change.

*Abstract from [MSc graduation thesis](http://resolver.tudelft.nl/uuid:8689a874-2460-4a80-b39a-70010d6562c9) ©️ 2022 by L. Keijser. Reprinted with permission.*

<div class="card">
    <p><strong>Page last Edited:</strong> 16/02/2024<br>
    <strong>Edited by:</strong> Dieter van Dortmont, Willemijn Elkhuizen<br>
    </p>
</div>