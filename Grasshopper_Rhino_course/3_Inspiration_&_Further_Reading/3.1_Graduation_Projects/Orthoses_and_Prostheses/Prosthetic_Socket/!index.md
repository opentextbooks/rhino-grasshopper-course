# Design of a 3D printed prosthetic socket that accommodates pediatric growth

```{tags} 3d-scans, Advanced-Manufacturing, personalized
```

<div class="card">
    <p><strong>Authors:</strong> T.A. Snijder<br>
    <strong>Supervisors:</strong> C.C. Wang (mentor), E.L. Doubrovski (mentor)<br>
    <strong>Graduation date:</strong> 24 May 2017<br>
    <strong>MSc thesis:</strong> Find it <a href="http://resolver.tudelft.nl/uuid:2e14b478-0d58-4486-b38b-243f5984b308"> here</a>
    </p>
</div>

```{tags} 3d-scans, Advanced-Manufacturing, personalized
```

```{figure} prosthetic_socket.jpeg
[Growth accomodating prosthetic socket](http://resolver.tudelft.nl/uuid:2e14b478-0d58-4486-b38b-243f5984b308) ©️ 2017 by T.A. Snijder. Reproduced with permission.
```


## **Abstract**

Inability to adapt to growth is a problem in the field of pediatric prostheses. Current prostheses do not adapt to the normal growth of children’s limbs and require constant visits to health care providers for adjustment or replacement. To solve this issue a new socket was designed making use of auxetic metamaterials. The residual limb of the pediatric user is scanned and a growth prediction is made based on the input scan. A thermo-adaptive socket is designed by 3D printing an auxetic cell structure of PCL which accommodates two years of growth. Polycaprolactone (PCL) was selected as the building material for the socket. With a melting temperature of 60ᵒC the socket can be adapted to the limb after being placed in a bath of water at 55ᵒC. A socket prototype was designed in collaboration with orthotics at De Hoogstraat.

*Abstract from [MSc graduation thesis](http://resolver.tudelft.nl/uuid:2e14b478-0d58-4486-b38b-243f5984b308) ©️ 2017 by T.A. Snijder. Reproduced with permission.*

<div class="card">
    <p><strong>Page last Edited:</strong> 16/02/2024<br>
    <strong>Edited by:</strong> Dieter van Dortmont, Willemijn Elkhuizen<br>
    </p>
</div>
