# Orthoses and Prostheses

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Medical_Hand_Splint/!index
:link-type: doc
:img-top: Medical_Hand_Splint/cover_crop.jpeg
:class-header: bg-light

Medical Hand Splint
:::


:::{grid-item-card}
:link: Prosthetic_Socket/!index
:link-type: doc
:img-top: Prosthetic_Socket/cover_crop.jpeg
:class-header: bg-light

Growth Accommodating Prosthetic Socket
:::

:::{grid-item-card}
:link: Ankle_Foot_Orthosis/!index
:link-type: doc
:img-top: Ankle_Foot_Orthosis/cover_crop.png
:class-header: bg-light

Ankle Foot Orthosis
:::

