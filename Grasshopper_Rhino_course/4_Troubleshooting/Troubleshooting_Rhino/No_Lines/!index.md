# Curves not visible in Rhino

On the (remote) desktop workstations at Industrial Design Engineering, we have run into the issue that lines drawn in the Rhino viewport are not visible (e.g. when getting started with lesson 1). 

To solve this, navigate to the settings window found under: Tools > Options > View > OpenGL (see top image). First check if the issue can be resolved by moving the slider labelled **level** to (all the way) to the left (see bottom image). If this does not work, *uncheck* the **GPU Tesselation** checkbox (see bottom image). 

<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Troubleshooting%20images/Tools_options.png?ref_type=heads"><br>
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Troubleshooting%20images/View,%20OpenGL.png?ref_type=heads">