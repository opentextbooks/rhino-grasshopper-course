# Creating a 3D model for fabrication

If you are using Rhino to design products that you plan on manufacturing, it is useful to check if your geometry is valid. You can do this by selecting an object in Rhino and looking at the properties tab in the menu. You can also use the command [**what**] to get a detailed object description. The image below shows what the object description might look like for a box. As you can see, this box is a closed solid polysurface, which means it is an closed volume that consists of multiple surfaces joined together. 

To join surfaces together into a polysurface, select the surfaces and use the [**join**] command. To un-join surfaces, you can use the [**explode**] command. This also works for curves and polycurves. 

A closed solid shape can be exported and used for digital fabrication, for example 3D printing. In lesson 4 we will discuss surfaces and solids in more detail. 

<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Troubleshooting%20images/1.4.1a_ObjectDescription.png?ref_type=heads">