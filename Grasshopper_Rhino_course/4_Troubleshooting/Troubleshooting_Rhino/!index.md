# Troubleshooting in Rhino

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: No_Lines/!index
:link-type: doc
:img-top: No_Lines/cover_crop.jpg
:class-header: bg-light

Curves not visible in Rhino
:::

:::{grid-item-card}
:link: Creating_3D_4_Fab/!index
:link-type: doc
:img-top: Creating_3D_4_Fab/Troubleshooting_cover_crop.png
:class-header: bg-light

Creating a 3D model for fabrication
:::

:::::