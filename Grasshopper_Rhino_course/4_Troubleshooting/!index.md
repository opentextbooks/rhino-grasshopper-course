# Troubleshooting

Here, you will find help and common problems in Rhino and Grasshopper.

<div class="dropdown-container">
    <details class="dropdown-details">
        <summary class="dropdown-summary">
            <strong>Contact for help</strong>
        </summary>
        <div class="dropdown-div">
            <p class="dropdown-p">
                If you detect any issues or want to provide feedback, please share them via GitLab or email at <a href="mailto:prototyping@tudelft.nl">prototyping@tudelft.nl</a>.        
            </p>
        </div>
    </details>
</div>

## [Rhino](/Grasshopper_Rhino_course/4_Troubleshooting/Troubleshooting_Rhino/!index.md)

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Troubleshooting_Rhino/No_Lines/!index
:link-type: doc
:img-top: Troubleshooting_Rhino/No_Lines/cover_crop.jpg
:class-header: bg-light

Curves not visible in Rhino
:::

:::{grid-item-card}
:link: Troubleshooting_Rhino/Creating_3D_4_Fab/!index
:link-type: doc
:img-top: Troubleshooting_cover_crop.png
:class-header: bg-light

Creating a 3D model for fabrication
:::

:::::

## [Grasshopper](/Grasshopper_Rhino_course/4_Troubleshooting/Troubleshooting_Grasshopper/!index.md)

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Troubleshooting_Grasshopper/Icon-Text/!index
:link-type: doc
:img-top: Troubleshooting_Grasshopper/Icon-Text/cover_crop.jpg
:class-header: bg-light

Component appearance: icons and (input/output) texts
:::

:::{grid-item-card}
:link: Troubleshooting_Grasshopper/Bifocals/!index
:link-type: doc
:img-top: Troubleshooting_Grasshopper/Bifocals/cover_crop.jpg
:class-header: bg-light

Bifocals: Add component names
:::

:::{grid-item-card}
:link: Troubleshooting_Grasshopper/Colours&Errors/!index
:link-type: doc
:img-top: Troubleshooting_Grasshopper/Colours&Errors/cover_crop.jpg
:class-header: bg-light

Understanding component colours and error messages
:::

:::::