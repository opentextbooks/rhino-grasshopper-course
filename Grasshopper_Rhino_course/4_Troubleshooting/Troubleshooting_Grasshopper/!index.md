# Troubleshooting in Grasshopper

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Icon-Text/!index
:link-type: doc
:img-top: Icon-Text/cover_crop.jpg
:class-header: bg-light

Component appearance: icons and (input/output) texts
:::

:::{grid-item-card}
:link: Bifocals/!index
:link-type: doc
:img-top: Bifocals/cover_crop.jpg
:class-header: bg-light

Bifocals: Add components names
:::

:::{grid-item-card}
:link: Colours&Errors/!index
:link-type: doc
:img-top: Colours&Errors/cover_crop.jpg
:class-header: bg-light

Understanding component colours and error messages
:::

:::::