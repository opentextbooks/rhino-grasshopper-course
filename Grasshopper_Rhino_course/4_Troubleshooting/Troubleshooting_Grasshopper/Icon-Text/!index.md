# Component appearance: icons and (input/output) texts

Some components can be hard to understand without seeing icons or the full text of the components' in -and outputs. 

You can switch between icons and text components in the <strong>[Display]</strong> menu at the top of the interface. You can also switch on <strong>[Draw full names]</strong> to make components easier to understand. 

<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Troubleshooting%20images/1.2.2d_GrasshopperDisplaysettings.png" style="margin-bottom: 10px;"><br>
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Troubleshooting%20images/Without_to_with_icons.png?ref_type=heads" alt="Components without names to with names and icons">