# Bifocals: Add components names

In order to see the names of your components, like used in our tutorials, you must install a plugin called 'Bifocals'. A short explanation of how to use it, along with installation files, can be found <a href="https://www.food4rhino.com/en/app/bifocals">here</a>.

<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Troubleshooting%20images/1.4.2.3_Bifocals_instructions.png?ref_type=heads"><br>