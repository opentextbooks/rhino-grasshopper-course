# Understanding component colours and error messages
If you run into issues in Grasshopper, take a closer look at your components to find out what is wrong. In general, if a component in Grasshopper is not colored grey, this means that it is not running correctly. An orange color indicates a warning, red indicates an error. 

You can hover your mouse over the middle of any component to find out what it does and if it is running. If you see a small speech bubble on a component, you can click it or hover your mouse over it to read the warning and error messages. Often, when you drop a new component, it will show up orange with the warning “input parameter failed to collect data”, like the example below. This simply means that you still need to connect something to the inputs in order for the component to run.

<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Troubleshooting%20images/1.4.2a_FailedToCollectData.png?ref_type=heads"><br>

Components can also turn red, which means there is an error. This might happen because you connected an input that the component cannot use. In the example below, you see a text panel used as an input, while the component asks for a vector to use as the direction for the extrusion. Grasshopper will try to convert the text into a useful input, but in this case it is unable to, which results in the error “Data conversion failed from Text to Vector”.

<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Troubleshooting%20images/1.4.2b_DataConversionFailed.png?ref_type=heads"><br>