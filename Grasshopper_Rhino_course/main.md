<div class="tip-details">
    <summary class="showcase-summary">
        <strong>🖱️ Exercise - Building shapes in Grasshopper</strong>
    </summary>
        <div class="tip-div">
            <p class="tip-p">
            In the first exercise we will be building a simple shape in Rhino and Grasshopper to explore how these two programs work and in what ways they differ. Now see if you can create the same cube in Grasshopper.
            </p>
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.2.3d_GrasshopperBoxRender.png?ref_type=heads" class="tip-img">
            <div class="hint-container">
                <details class="tip-details">
                    <summary class="hint-summary">
                        <strong>👀Hint:</strong> Grasshopper Cube
                    </summary>
                    <div class="tip-div">
                        <p class="tip-p">
                            Use the components called <strong>[Box 2pt]</strong> and <strong>[Construct Point]</strong>.        
                        </p>
                        <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.2.3e_GrasshopperBoxHint.png?ref_type=heads" class="tip-img">
                    </div>
                </details>
            </div>
            <div class="tip-container">
                <details class="tip-details">
                    <summary class="tip-summary">
                        <strong> Solution</strong>
                    </summary>
                    <div class="tip-div">
                        <p class="tip-p">
                            Grasshopper always allows for many different approaches to solve a problem. It is perfectly fine if you managed to get the same end result with a different script!
                            Here we will show one possible approach:     
                        </p>
                        <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.2.3f_GrasshopperBoxSolution.gif?ref_type=heads" alt="Gitlab" class="tip-img">
                    </div>
                </details>
            </div>
        </div>
    </div>
</div>