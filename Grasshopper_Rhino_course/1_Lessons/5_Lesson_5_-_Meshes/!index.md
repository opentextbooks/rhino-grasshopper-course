# Lesson 5 - Meshes

```{tags} Meshes, Patterns, Rendering
```
<div class="card">
    <p><strong>Authors:</strong> Anne van den Dool<br>
    <strong>Last Edited:</strong> 14/02/2024<br>
    <strong>Edited by:</strong> Dieter van Dortmont</p>
</div>

## 👩‍🏫 5.1 Introduction (5 min)

<iframe width="560" height="315" src="https://www.youtube.com/embed/4thBvk4j4cI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## 📑🖱️5.2 Mesh geometry (5 min)

<div class="card">
    <p><strong>📌 What:</strong> Explanation mesh geometry (reading) and exploration of mesh tools in Rhino (exercise)</p>
    <p><strong>🙋 For Whom:</strong> Intermediate in Rhino/Grasshopper</p>
    <p><strong>⌛ Time:</strong> 10 minutes</p>
</div>

### 5.2.1 📑Mesh vs. Surface geometry

A mesh is a type of geometry that consists of points and faces. It can be used for similar purposes as a Brep geometry, but works in a fundamentally different way. A Brep datatype is a mathematical representation of the object, while a mesh datatype only is a collection of points and its connectivity data. In the image below you can see a comparison between a NURB surface and a mesh. When changing the control point on a curved surface, the surface is recalculated and remains smooth. When you change a point in a mesh, this only affects the face that are directly connected to that point. 

![5.2.1a_SurfacevsMesh.gif](5.2.1a_SurfacevsMesh.gif)

The points in a mesh are called **vertices** and the surfaces that are created by connecting these points are called **faces**. Each mesh face also has a **normal** direction. If a mesh is a closed shape, it is considered a solid object and all the normals will point outwards. 

![5.2.1b_MeshGeometry.png](5.2.1b_MeshGeometry.png)

Meshes take less computational power to visualize, but since meshes only consist of points and faces, curved shapes will never look completely smooth. The higher the resolution of your mesh, the higher the amount of faces and the smoother it will look. You can export meshes from Rhino as STL or OBJ file for digital fabrication.

![5.2.1c_MeshResolution.png](5.2.1c_MeshResolution.png)

### 5.2.2 📑Mesh structure

There are two common types of meshes: triangle meshes and quad meshes. As the names suggest, a triangle mesh is a mesh with triangular faces (connecting 3 vertices) and a quad mesh is a mesh with rectangular faces (connecting 4 vertices). A combination of triangle faces and quad faces in a mesh is also possible. 

![5.2.2a_MeshTypes.png](5.2.2a_MeshTypes.png)

Imagine you want to create a triangle mesh based on 5 points. As you can see in the animation below, there are many ways to connect these points in to faces. In Grasshopper, you can describe a triangle face with the index numbers of the vertices that are connected by the face. For example, a face that connects point 0, 1 and 2 would be describes as **T{0;1;2}**. The T stands for triangle face. In a similar way, you can describe a quad face with four numbers, for example **Q{0;1;2;3}.** In tutorial 5.3 we will further explore how to build a mesh by connecting a collection of points. 

![5.2.2b_MeshFaces.gif](5.2.2b_MeshFaces.gif)

![5.2.2c_GrasshopperMeshGeometry.png](5.2.2c_GrasshopperMeshGeometry.png)

### 5.2.3 🖱️Mesh tools in Rhino

<div class="exercise-container">
    <div class="tip-details">
        <summary class="exercise-summary">
            <strong>🖱️ Exercise</strong>
        </summary>
        <div class="tip-div">
            <p>Mesh files like 3D scans often need some repairing or editing. You can use specialized software like Meshmixer, but you can also find some mesh tools in Rhino.
            <br><br>
            🖱️Open Rhino and go to the [<strong>Mesh Tools</strong>] menu. Experiment with the toolbar on the left to create some basic mesh shapes. You can combine shapes by using Mesh Boolean commands, for example by typing in [<strong>MeshBooleanUnion</strong>] or [<strong>MeshBooleanDifference</strong>]. You can also select these tools from the mesh toolbar.
            <br><br>
            🖱️Create a flat surface and position it through your mesh shape. Select the surface and use [<strong>MeshTrim</strong>] to remove a part of your mesh. This can be very useful when you are working with a large scan and you only want to work with a certain part. You can use the command [<strong>Cap</strong>] to close your mesh, but this will only work if the gap in the mesh is planar.
            <br><br>
            🖱️Use the [<strong>PointsOn</strong>] command to visualize and manipulate individual points on you mesh. You can select them and use the gumball to move them around. Use the [<strong>PointsOff</strong>] command to switch the points off again.
            <br><br>
            🖱️Once you have created an interesting shape, select it and use the [<strong>ReduceMesh</strong>] command to change the resolution of the mesh. You can specify how many face you would like your mesh to have, or specify a percentage to reduce by.
            <br><br>
            🖱️Select your mesh and run the [<strong>MeshRepair</strong>] command. In the pop-up window, select Check Mesh. You will now receive a analysis of your mesh. If your mesh has issues, like open edges (naked edges) you can try to repair them with this tool. Click on Repair Mesh to automatically repair, or click on Next to select which issues you want to repair. If you want to use your mesh for digital production, you need to make sure your mesh is a valid closed mesh. </p>
            <div class="image-container">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_5/5.2.3a_MeshTools.png?ref_type=heads" alt="Gitlab" class="tip-img">
            </div>
        </div>
    </div>
</div>

---

## 📺 5.3 Building a custom mesh (25 min)

<div class="card">
    <p><strong>📌 What:</strong> Understanding mesh geometry (tutorial)</p>
    <p><strong>🙋 For Whom:</strong> Intermediate in Rhino/Grasshopper</p>
    <p><strong>⌛ Time:</strong> 25 minutes</p>
</div>

<div class="tutorial-container">
    <div class="tip-details">
        <summary class="tutorial-summary">
            <strong>📺 Tutorial</strong>
        </summary>
        <div class="tip-div">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/pY7N2-3gySI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <div class="tutorial-container">
                <details class="tip-details">
                    <summary class="tutorial-summary">
                        <strong> Download starting file and material file here</strong>
                    </summary>
                    <div class="tip-div">
                        <p class="tip-p">

[Starting file](Lesson_5_-_Custom_mesh_starting_file.3dm)<br>
[Material file](Lesson_5_-_Render_materials.gh)
                        </p>
                    </div>
                </details>
            </div>
            <div class="card">
                📎 During the tutorial we will create a custom mesh, using a list of faces. You can type them out, or copy the list below into your grasshopper definition.<br>
                0;1;3<br>
                1;2;5<br>
                1;5;4<br>
                1;4;3<br>
                3;7;6<br>
                3;4;7<br>
                4;5;7<br>
                5;8;7<br>
            </div>
            <div class="tip-container">
                <details class="tip-details">
                    <summary class="tip-summary">
                        <strong>💡Tip:</strong> Switching files
                    </summary>
                    <div class="tip-div">
                        <p class="tip-p">
                            If you already have Rhino and Grasshopper open and you want to open another Grasshopper file, like the render materials, simply drag and drop the GH file into Rhino. You can switch between Grasshopper files by clicking on the file name in the top right corner.         
                        </p>
                        <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_5/5.3a_SwitchingFiles.png?ref_type=heads" alt="Gitlab" class="tip-img">
                    </div>
                </details>
            </div>
        </div>
    </div>
</div>

---

## 📺 5.4 Morphing (30 min)

<div class="card">
    <p><strong>📌 What:</strong>Building mesh patterns and morphing onto a surface (tutorial)</p>
    <p><strong>🙋 For Whom:</strong> Intermediate in Rhino/Grasshopper</p>
    <p><strong>⌛ Time:</strong> 30 minutes</p>
</div>

<div class="exercise-container">
    <div class="tip-details">
        <summary class="exercise-summary">
            <strong>🖱️ Exercise</strong>
        </summary>
        <div class="tip-div">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/oSn4pJ35rJo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <div class="card">
                <p>Plugins mentioned in the video:<br>
                <a href="https://www.giuliopiacentino.com/weaverbird/">Weaverbird Plugin</a><br>
                <a href="https://grasshopperdocs.com/addons/grasshopper-vector.html">Grasshopper Docs</a>
            </div>
        </div>
    </div>
</div>

---

## 💻 5.5 Assignment lesson 5 (max 2 hours)

<div class="assignment-container">
    <div class="tip-details">
        <summary class="assignment-summary">
            <strong>💻 Assignment </strong>
        </summary>
        <div class="tip-div">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_5/5.5a_Assignment5.png?ref_type=heads" alt="Gitlab" class="tip-img">
            <div class="card">
                💻 Model a glass with a pattern on part of it. Use the Box Morph component in Grasshopper to morph the pattern onto your product. You can choose if you want to model the glass fully in Grasshopper, or use Rhino for part of the process. The pattern can be any shape you like. Use custom materials to create a nice render of your final product.
            </div>
        </div>
    </div>
</div>
