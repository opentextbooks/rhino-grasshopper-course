# Lesson 1 - Basics

```{tags} Introduction
```

<div class="card">
    <p><strong>Authors:</strong> Anne van den Dool<br>
    <strong>Last Edited:</strong> 14/02/2024<br>
    <strong>Edited by:</strong> Niels van Silfhout, Dieter van Dortmont</p>
</div>

## 👩‍🏫1.1 Introduction (3 min)

<iframe width="560" height="315" src="https://www.youtube.com/embed/YeSgIJmp9OY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### 📑 1.1.1 Rhino: Installing Rhino and Grasshopper (10 min)

To commence your journey into 3D modeling with Rhino Grasshopper, you will need to install the software. You can locate the installation files and  instructions on [software.tudelft.nl](https://software.tudelft.nl) (for TUDelft students) or via [rhino3d.com](https://www.rhino3d.com). 

<div class="dropdown-container">
    <details class="dropdown-details">
        <summary class="dropdown-summary">
            <strong>Rhino version and reporting issues</strong>
        </summary>
        <div class="dropdown-div">
            <p class="dropdown-p">
                In this course, we use Rhino 7. If you detect any issues or want to provide feedback, please share them via GitLab or email at <a href="mailto:prototyping@tudelft.nl">prototyping@tudelft.nl</a>.        
            </p>
        </div>
    </details>
</div>

---

## 📑1.2 Basic controls (30 min)

<div class="card">
    <p><strong>📌 What:</strong> Introduction to the controls of Rhino and Grasshopper (reading), working in Rhino and Grasshopper (exercise)</p>
    <p><strong>For Whom:</strong> Beginners in Rhino/Grasshopper</p>
    <p><strong>Time:</strong> 30 min (15 min reading + 15 min exercise)</p><br>
    <p>If you are new to 3D design software, using Rhino and Grasshopper for the first time can be a bit overwhelming. In the next section, you will first walk through the ins and outs of the user interface, then you will learn the basic controls. Finally ending with a small exercise to get you started with Rhino and Grasshopper.</p>
</div>

### 📑 1.2.1 Rhino: User Interface (Reading 10 min)

<div class="card">
In this part you will explore Rhino’s User Interface.
</div>

🖱️Open Rhino

You will now see the following interface (left for 🪟windows, right for 🍎mac):

![1.2.1a_RhinoInterface_V2.png](1.2.1a_RhinoInterface_V2.png)

The central part of the screen is called the viewport, here marked in red. By default Rhino shows 4 different views. The viewport is your workspace, here you can view your 3D model.

<div class="tip-nodropdown">
    <strong>💡 Tip:</strong> To make one of the viewports fill the entire window, 🖱️double click the label (for example: <strong>[Perspective]</strong>). To return to the view of all four viewports, 🖱️double click the label again.
</div>

At the top there are different tabs for different workflows, for example **[Curve Tools]**. By clicking these tabs you gain access to more specialized tools, and the lay-out of bar on the left and top changes. We will mostly be using the **[Standard]** tab, which is selected by default.

![1.2.1b_RhinoToolbars.png](1.2.1b_RhinoToolbars.png)

All of the different tools in Rhino can be accessed via the bar on the left. By clicking the triangle in the corner of some of the icons, you have access to even more options.

![1.2.1c_ToolbarCloseup.png](1.2.1c_ToolbarCloseup.png)

![1.2.1d_RhinoTools.png](1.2.1d_RhinoTools.png)

1. This is the **[Command:]** line, another way of accessing all the tools in Rhino. The **[Command:]** line is an intelligent tool, and depending on the input can do many different things, more on that later.

2. By clicking the 🔽 next to a viewport (or right click if you’re working on mac), you can adjust how your model looks. Most of the time you will use **[Shaded]**.

3. **[Gumball]** allows you to move objects directly in the viewport using handles. **[Grid Snap]**  **[Osnap]** causes tools to snap to the grid and to other objects. **[Record History]** turning this on allows you to undo and redo actions.

<div class="dropdown-container">
    <div class="dropdown-details">
        <summary class="dropdown-summary">
            <strong>Navigation</strong>
        </summary>
        <div class="dropdown-div">
            <table class="dropdown-table">
                <tr>
                    <th>Rotate</th>
                    <td>Hold right mouse button</td>
                </tr>
                <tr>
                    <th>Pan</th>
                    <td>Right mouse button + shift</td>
                </tr>
                <tr>
                    <th>Zoom</th>
                    <td>Scroll</td>
                </tr>
                <tr>
                    <th>Select</th>
                    <td>Left click</td>
                </tr>
                <tr>
                    <th>Box select</th>
                    <td>Hold left mouse button and drag. Drag from right to left to select everything the box touches. Drag from left to right to select only objects completely inside box</td>
                </tr>
                <tr>
                    <th>Repeat previous command</th>
                    <td>Right click</td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div class="tip-container">
    <details class="tip-details">
        <summary class="tip-summary">
            <strong>💡Tip:</strong> More Rhino Shortcuts
        </summary>
        <div class="tip-div">
            <p class="tip-p">
                <a href="https://docs.mcneel.com/rhino/mac/help/en-us/user_interface/shortcuts.htm" target="_blank">
                    <button name="button">For Mac 🍎</button>
                </a>
                <a href="https://docs.mcneel.com/rhino/7/help/en-us/user_interface/shortcuts.htm" target="_blank">
                    <button name="button">For Windows 🪟</button>
                </a>
            </p>
        </div>
    </details>
</div>

### 📑 1.2.2 Grasshopper: User Interface (Reading 5 min)

<div class="card">
In this part we will look at the basic controls and interface of Grasshopper. The Grasshopper interface looks the same on each operating system.
</div>

To open Grasshopper:

🖱️Click the **[Launch Grasshopper]** button as shown below

**OR**

⌨️Type “Grasshopper” in the **[Command:]** line and press Enter

![1.2.2a_LaunchGrasshopper.png](1.2.2a_LaunchGrasshopper.png)

The Grasshopper interface is quite straight forward. For now the most important parts are:

1. The **component library**, including tabs for any plugins you might have installed. 

2. The **canvas**, where you build your script.

3. The **file-manager**, where you can switch between documents.

![1.2.2b_GrasshopperInterface.png](1.2.2b_GrasshopperInterface.png)

Grasshopper is a visual coding tool. This means that you create a 3D model by connecting different functional blocks together called components.

This way you can build a script which takes certain inputs to produce the desired output.

![1.2.2c_GrasshopperWorkflow.png](1.2.2c_GrasshopperWorkflow.png)

Every component has inputs and outputs. Inputs are on the left side of a block, outputs on the right side.

<div class="tip-container">
    <details class="tip-details">
        <summary class="tip-summary">
            <strong>💡Tip:</strong> Switch between icons and text
        </summary>
        <div class="tip-div">
            <p class="tip-p">
                You can switch between icons and text components in the <strong>[Display]</strong> menu at the top of the interface. You can also switch on <strong>[Draw full names]</strong> to make components easier to understand.         
            </p>
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.2.2d_GrasshopperDisplaysettings.png" alt="Gitlab" class="tip-img">
        </div>
    </details>
</div>

#### Placing and connecting components

To place a component, either 🖱️drag from the toolbar or 🖱️double click the workspace to search by name. 

![1.2.2e_DragingComponents.gif](1.2.2e_DragingComponents.gif)

![1.2.2f_SearchingComponents.gif](1.2.2f_SearchingComponents.gif)

<div class="tip-nodropdown">
    <strong>💡Tip:</strong> To display the speech bubbles with the names of the components, you need to install a plugin called <a href="../../4_Troubleshooting/Troubleshooting_Grasshopper/Bifocals/%21index.html">“Bifocals”</a>. This and more troubleshooting can be found at <a href="../../4_Troubleshooting/%21index.html">Troubleshooting</a>.
</div>



To connect components, simply 🖱️drag from an output of one component, to the input of the next. To undo a connection, 🖱️hold CTRL (or Command on Mac) and drag from the output to the input.

![1.2.2g_ConnectingComponents.gif](1.2.2g_ConnectingComponents.gif)

#### Inputting and Outputting Data

When hovering your cursor over an input/output a tool-tip pops up. These tool-tips can help determine what needs to be connected where. Reading out the outputs can help verify the expected outcome and check for errors.

![1.2.2h_Tooltips.gif](1.2.2h_Tooltips.gif)

One of the basic components we will use often as an input in Grasshopper is a Number Slider. You can find them in the Input menu under params. 

By double clicking the slider, a popup appears with settings. Here you can change the name of the slider, set the number of digits and change the minimum and maximum values.

![Rhino grasshopper lesson 1 intro slider.png](Rhino_grasshopper_lesson_1_intro_slider.png)

<div class="tip-container">
    <details class="tip-details">
        <summary class="tip-summary">
            <strong>💡Tip:</strong> A faster way to create your sliders
        </summary>
        <div class="tip-div">
            <p class="tip-p">
                For a faster way to create sliders, you can type a number in the search bar to create a slider. You can also create a slider with a set range by typing the minimum followed by two dots (..) and the maximum number. The amount of digits you use to type the numbers will determine the accuracy of the slider.        
            </p>
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.2.2i_SliderTip%20copy.gif?ref_type=heads" alt="Gitlab" class="tip-img">
        </div>
    </details>
</div>

You can also use Rhino geometry as an input in Grasshopper. For example if you wanted use a curve as input: 

🖱️ Draw a curve in Rhino, for example by selecting **[Control Points Curve]** from the toolbar on the left.

🖱️ Place a **[Curve]** component on the canvas in Grasshopper. You can do this by dragging it from the Params/Geometry toolbar or by double clicking on the canvas and searching for the **[Curve]** component. 

🖱️ Right click in the middle of the component and select **[set one curve].**

A prompt appears in Rhino, asking you which Curve or Edge to reference. On windows this prompt will appear above the command line, in mac you will find it in the menu below the command line. 

🖱️ Select the curve in Rhino by left clicking on it.

After selecting you curve, the curve component should turn green, indicating that it now contains geometry. 

![1.2.2j_SetCurve.png](1.2.2j_SetCurve.png)

#### Exporting shapes from Grasshopper to Rhino

Shapes built in Grasshopper show up as red translucent objects in Rhino. These red objects are placeholders, and can not be manipulated directly in Rhino.

To edit your Grasshopper models in Rhino you need to export the model. This type of exporting from Grasshopper to Rhino is called “baking”.

To bake an object from Grasshopper to Rhino:

🖱️Right click the component you want to bake. Make sure you click in the middle.

🖱️Click **[Bake…]**.

🖱️ You will be asked on which layer you want to bake the object. Select **[OK]**.

You should now see your object in Rhino.

![1.2.2k_BakeToRhino.png](1.2.2k_BakeToRhino.png)

<div class="tip-nodropdown">
    <strong>💡Tip:</strong> Once an object is baked from Grasshopper to Rhino, you can no longer change it using the Grasshopper script (you can’t unbake an egg!). If you make changes in Grasshopper, you will need to bake it again.
</div> 

<div class="exercise-container">
    <div class="tip-details">
        <summary class="exercise-summary">
            <strong>🖱️ Exercise - Building shapes in Rhino (15 min)</strong>
        </summary>
        <div class="tip-div">
            <p>In the first exercise we will be building a simple shape in Rhino and Grasshopper to explore how these two programs work and in what ways they differ.</p>
            <p>Try to create a cube in Rhino with the dimensions 10x10x10mm.</p>
            <div class="note">
                <strong> ✏️ Note:</strong> You do not need to use Grasshopper yet.
            </div>
            <div class="image-container">
                <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.2.3a_RhinoBoxRender.png?ref_type=heads" alt="Gitlab" class="tip-img">
                <div class="hint-container">
                    <details class="tip-details">
                        <summary class="hint-summary">
                            <strong>👀Hint:</strong> Cubes in Rhino
                        </summary>
                        <div class="tip-div">
                            <p class="tip-p">A cube is called <strong>"box"</strong> in Rhino.</p>
                        </div>
                    </details>
                </div>
            </div>
            <div class="solution-container">
                <details class="tip-details">
                    <summary class="exercise-summary">
                        <strong> Solution</strong>
                    </summary>
                    <div class="tip-div">
                        <p class="tip-p">You can either select the **[Box]** tool on the left **OR** type in “Box” in the command line.</p>
                        <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.2.3b_RhinoBoxSolution.png?ref_type=heads" alt="Gitlab" class="tip-img">
                        <p class="tip-p">Now you can select the first corner of the base in the viewport, then the second corner, and finally choose the height of your box. You can also type the exact dimensions into the command line.</p>
                        <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.2.3c_RhinoBoxSolution.gif?ref_type=heads" alt="Gitlab" class="tip-img">
                    </div>
                </details>
            </div>
        </div>
    </div>
</div>

<div class="exercise-container">
    <div class="tip-details">
        <summary class="exercise-summary">
            <strong>🖱️ Exercise - Building shapes in Grasshopper</strong>
        </summary>
        <div class="tip-div">
            <p>Now see if you can create the same cube in Grasshopper</p>
            <div class="image-container">
                <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.2.3d_GrasshopperBoxRender.png?ref_type=heads" alt="Gitlab" class="tip-img">
                <div class="hint-container">
                    <details class="tip-details">
                        <summary class="hint-summary">
                            <strong>👀Hint:</strong> Cubes in Grasshopper
                        </summary>
                        <div class="tip-div">
                            <p class="tip-p">Use the components called <strong>[Box 2pt]</strong> and <strong>[Construct Point]</strong>.</p>
                            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.2.3e_GrasshopperBoxHint.png?ref_type=heads" class="tip-img">
                        </div>
                    </details>
                </div>
            </div>
            <div class="solution-container">
                <details class="tip-details">
                    <summary class="exercise-summary">
                        <strong> Solution</strong>
                    </summary>
                    <div class="tip-div">
                        <p class="tip-p">Grasshopper always allows for many different approaches to solve a problem. It is perfectly fine if you managed to get the same end result with a different script!</p>
                        <p class="tip-p">Here we will show one possible approach:</p>
                        <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.2.3f_GrasshopperBoxSolution.gif?ref_type=heads" alt="Gitlab" class="tip-img">
                    </div>
                </details>
            </div>
        </div>
    </div>
</div>

## 👩‍🏫1.3 Basic geometry (45 min)

<div class="card">
    <p><strong>📌 What:</strong> Introduction to the basic geometries (video explanation), modelling geometry in Rhino and Grasshopper (video tutorials)</p>
    <p><strong>For Whom:</strong> Beginners in Rhino/Grasshopper</p>
    <p><strong>Time:</strong> 45 min (5 min video+ 40 min tutorials)</p>
</div>

### 👩‍🏫1.3.1 Theory - Basic geometry (5 min)

<iframe width="560" height="315" src="https://www.youtube.com/embed/TOz-LE0H104" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<div class="dropdown-container">
    <details class="dropdown-details">
        <summary class="dropdown-summary">
            <strong>Recap</strong>
        </summary>
        <div class="dropdown-div">
        
[Download Slides PDF](1_Basic_geometry.pdf)
        </div>
    </details>
</div>

### 📺1.3.2 Tutorial - Modelling a teapot in Rhino (20 min)

<div class="tutorial-container">
    <div class="tip-details">
        <summary class="tutorial-summary">
            <strong>📺 Tutorial</strong>
        </summary>
        <div class="tip-div">
            <p>In this tutorial, you will learn how to construct basic geometry in Rhino. Watch the video below and follow along.</p>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/38Xfl50o2A8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <div class="tip-container">
            <details class="tip-details">
                <summary class="tip-summary">
                    <strong>💡Tip:</strong> Viewport display options
                </summary>
                <div class="tip-div">
                    <p class="tip-p">
                    By default, the viewports in Rhino are set to wireframe mode. This means you only see the outlines of objects. You can switch to shaded mode by clicking on the  🔽 next to a viewport (or right click if you’re working on mac).     
                    </p>
                </div>
            </details>
        </div>       
        </div>
    </div>
</div>

### 📺1.3.3 Tutorial - Modelling teacups in Grasshopper (20 min)

<div class="tutorial-container">
    <div class="tip-details">
        <summary class="tutorial-summary">
            <strong>📺 Tutorial</strong>
        </summary>
        <div class="tip-div">
            <p>In this exercise, you will learn how to construct basic geometry in Grasshopper. Watch the video below and follow along. If you get stuck, you can check out the full solution below.</p>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/svhn73Yvww0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <div class="warning-container">
                <details class="tip-details">
                    <summary class="warning-summary">
                        <strong>⚠️Caution:</strong> Pay attention to where you click
                    </summary>
                    <div class="tip-div">
                        <p class="tip-p">
                            In Grasshopper, pay attention where you click. If you right click on the inputs of a component, you will get a different menu compared to when you click in the middle for example. If you want to group object or turn the preview of for multiple object, make sure you right click on a empty space on the canvas.         
                        </p>
                    </div>
                </details>
            </div>
            <div class="tutorial-container">
                <details class="tip-details">
                    <summary class="tutorial-summary">
                        <strong> Solution</strong>
                    </summary>
                    <div class="tip-div">
                        <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.3.3a_TeacupSolution_V2.png?ref_type=heads" alt="Gitlab" class="tip-img">
                    </div>
                </details>
            </div>
        </div>
    </div>
</div>

---

## 📑1.4 Best practice (5 min)

<div class="card">
    <p><strong>📌 What:</strong> Some tips and tricks to help you troubleshoot in Rhino/Grasshopper (reading)</p>
    <p><strong>For Whom:</strong> Beginners in Rhino/Grasshopper</p>
    <p><strong>Time:</strong> 5 minutes</p>
</div>

### 📑1.4.1 How to keep you Grasshopper script organized

As projects get more complex, it is important to keep your Grasshopper script organized. Below are a few tips to keep your scrip readable (for yourself and others):

1. Work from left to right. Inputs should be put on the left, outputs on the right.

2. Keep connectors as straight as possible.

3. Turn off previews for components which you are not working on. This can be done by opening the right-click menu and clicking “Preview Off” while having components selected.

4. Group components by function. You can do this by selecting the components and using the shortcut CTRL+G (or Command+G on mac).

5. Label your groups. Use the component **[Scribble]** to add text

### 📑1.4.2 Troubleshooting in Rhino & Grasshopper

Got stuck designing a working model/script using Rhino and/or Grasshopper? Under "[Troubleshooting](../../../Grasshopper_Rhino_course/4_Troubleshooting/!index.md)" you can find some some of our tips for frequently asked questions. Still stuck after these tips? You can also check out some links under "[External Links](../../../Grasshopper_Rhino_course/3_Inspiration_&_Further_Reading/3.2_External_Links/!index.md)" (Inspirational and further reading). If still no solution: try Googling (dedicated Grasshopper web forums), AI tools (e.g. ChatGPT), or of course human experts (e.g. teaching assistants, teachers, etc.).

---

## 💻1.5 Assignment lesson 1 (max 2 hours)

<div class="assignment-container">
    <div class="tip-details">
        <summary class="assignment-summary">
            <strong>💻 Assignment </strong>
        </summary>
        <div class="tip-div">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.5a_Assignment1.png?ref_type=heads" alt="Gitlab" class="tip-img">
            <div class="card">
                <p>💻 Create a creature using Rhino curves, which you import into Grasshopper. It does not have to be an insect, you can create any animal you like. Take a few hours to see what you can create, but do not spend to much time on this assignment.</p>
            </div>
            <div class="tip-container">
                <details class="tip-details">
                    <summary class="tip-summary">
                        <strong>💡Tip:</strong> Exporting a high-resolution image
                    </summary>
                    <div class="tip-div">
                        <p class="tip-p">
                            Use the <strong>[Export Hi-Res Image]</Strong> option in the file menu to create a detailed screenshot of your entire script.         
                        </p>
                        <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_1/1.5b_ExportImageTip.png?ref_type=heads" alt="Gitlab" class="tip-img">
                    </div>
                </details>
            </div>
        </div>
    </div>
</div>

<div class="tip-details">
    <summary class="showcase-summary">
        <strong>🎓Student Showcase:</strong> Check out the student showcase to see what other students made for this assignment
    </summary>
    <div class="tip-div">
    <details class="showcase-details" open>
        <summary class="showcase-summary">
            <strong>🎓2022</strong>
        </summary>
        <div class="gallery">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/1.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/10.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/11.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/12.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/13.jpeg">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/14.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/2.jpg">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/3.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/4.jpeg">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/5.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/6.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/7.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/8.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_/9.png">
        </div>
        <p style="text-align: center; color: #888; font-style: italic; padding-top: 20px;">With permission from students year 2022</p>
    </details>
    <details class="showcase-details" open>
        <summary class="showcase-summary">
            <strong>🎓2021</strong>
        </summary>
        <div class="gallery">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_/1.PNG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_/10.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_/11.PNG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_/12.PNG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_/13.jpeg">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_/2.jpg">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_/3.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_/4.jpg">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_/5.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_/6.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_/7.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_/8.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_/9.png">
        </div>
        <p style="text-align: center; color: #888; font-style: italic; padding-top: 20px;">With permission from students year 2021</p>
    </details>
    <details class="showcase-details" open>
        <summary class="showcase-summary">
            <strong>🎓2020</strong>
        </summary>
        <div class="gallery">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/1.JPG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/10.PNG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/11.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/12.PNG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/13.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/14.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/2.PNG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/3.jpg">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/4.PNG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/5.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/6.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/7.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/8.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_/9.PNG">
        </div>
        <p style="text-align: center; color: #888; font-style: italic; padding-top: 20px;">With permission from students year 2020</p>
    </details>
    <details class="showcase-details" open>
        <summary class="showcase-summary">
            <strong>🎓2019</strong>
        </summary>
        <div class="gallery">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/1.PNG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/2.JPG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/3.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/4.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/5.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/6.PNG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/7.JPG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/8.PNG">
        </div>
        <p style="text-align: center; color: #888; font-style: italic; padding-top: 20px;">With permission from students year 2019</p>
    </details></div>
</div>