# Lesson 4 - Surfaces

```{tags} Patterns, Surfaces
```

<div class="card">
    <p><strong>Authors:</strong> Anne van den Dool, Jose Martinez Castro<br>
    <strong>Last Edited:</strong> 14/02/2024<br>
    <strong>Edited by:</strong> Niels van Silfhout, Dieter van Dortmont</p>
</div>

## 👩‍🏫 4.1 Introduction (2 min)

<iframe width="560" height="315" src="https://www.youtube.com/embed/aR36XrEsxMk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 📑 4.2 Surface geometry (10 min)

<div class="card">
    <p><strong>📌 What:</strong> Explanation of surface geometry (reading)</p>
    <p><strong>🙋 For Whom:</strong> Beginners in Rhino/Grasshopper</p>
    <p><strong>⌛ Time:</strong> 10 minutes</p>
</div>

### 📑4.2.1 Breps and solid operations

Remember the tea cup you made in [Lesson 1 - Basics](../1_Lesson_1_-_Basics/!index.md)? Although it might look quite simple, the tea cup is made up of many different surfaces.

![4.2.1a_TeacupWithText.png](4.2.1a_TeacupWithText.png)

These surfaces together make up a volume, or a so called Boundary Representation (Brep). Breps are intuitive to work with, as they act like objects from the real world.

Using Breps you can easily make all kinds of objects by just adding on or cutting away other shapes. Say for example you want to turn your tea cup into a candle holder. By just creating a cylinder through the tea cup, and using the component **[Solid Difference]**, you have now created your own candle holder.

![4.2.1b_CandleHolder.png](4.2.1b_CandleHolder.png)

But what if you want to create a candle holder with a lot of holes? Maybe you would even like to add a pattern to the holes?

Using what you learned from [Lesson 3 - Data Structures](../3_Lesson_3_-_Data_structures/!index.md), you can construct a list of cylinders and rotate and move them however you would like. Now take the list of cylinders and apply the **[Solid Difference]** component from before. You should end up with something like the picture below.

![4.2.1c_CandleholderManyHoles.png](4.2.1c_CandleholderManyHoles.png)

If you did everything correctly, you might have noticed that your Grasshopper seized up for a moment. Operations between Breps, such as **[Solid Difference]**, are incredibly computationally heavy, especially when using a lot of shapes.

In Grasshopper you can see how much time each component takes to calculate. In this case the **[Solid Difference]** component took over 20 seconds!

![4.2.1d_SolidDifferenceProfiler.png](4.2.1d_SolidDifferenceProfiler.png)

Long computations like this are acceptable for finishing touches on an almost completed design. If you are still actively working on an object however, any small adjustment you make is going to take 20 seconds to calculate.

<div class="tip-container">
    <details class="tip-details">
        <summary class="tip-summary">
            <strong>💡Tip:</strong> Calculation time visibility
        </summary>
        <div class="tip-div">
            <p class="tip-p">
                Can’t see the calculation time? Enable the <strong>Profiler</strong> widget in the Grasshopper settings!      
            </p>
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_4/4.2.1e_ProfilerSettings.png?ref_type=heads" alt="Gitlab" class="tip-img">
        </div>
    </details>
</div>

### 📑4.2.2 Single surfaces

In order to create complex patterns and geometries, it is important to understand the basic building block of Breps: a single surface. Single surfaces are not like anything you can encounter in the real world. Surfaces have an area, but no thickness; they are infinitely thin. The shape of a surface is defined by the curvature degree and the number of control points in two directions. The two directions are a type of coordinate system on the surface. The directions on a surface are called the U- and V-directions. Any point on a surface can be described with a (U,V) coordinate. The curves you see on a surface in the U- and V-directions are called Isocurves. 

![4.2.2a_UVsurfaceWithAxis.png](4.2.2a_UVsurfaceWithAxis.png)

Surfaces also have a third direction, which is called the normal direction or the W-direction. The normal direction is a vector pointing outwards from the surface. Each surface has a front and a backside, which is determined by the direction of the normals. The front is the side where the normals point outwards. To visualize the direction of a surface in Rhino, you can use the **[Dir]** command. In this command, you also have the option to flip the surface and reverse the U and V directions. 

![4.2.2b_SurfaceNormals.png](4.2.2b_SurfaceNormals.png)

If you would create a hole in this surface, like the candle holder example in the previous paragraph, this creates a trimmed surface. If you turn on the control points (use the **[PointsOn]** command), you will notice that a trimmed surface has the same UV-grid as the untrimmed surface. The original structure of the surface is always preserved in a trimmed surface. 

![4.2.2c_SurfaceTrim.png](4.2.2c_SurfaceTrim.png)

Just like a curve has a domain with a start and end point, a surface has a domain in two directions and edges. You can also **[Reparameterize]** surfaces in Grasshopper to make sure the domain of the surface is between 0 and 1. On a reparameterized surface, each point on the surface will have a (U,V) coordinate between (0,0) and (1,1). 

A single surface usually looks like a rectangle, but you can also transform it into more complex shapes. Geometries like a sphere and a open cylinder can also be created with a single surface. In these shapes, the surface connects back to itself, creating a **seam**.

![4.2.2d_SphereCylinder.png](4.2.2d_SphereCylinder.png)

The control points and UV-structure make single surfaces quite easy to work with. You can map complex patterns to a surface by using the (U,V) coordinates and normal direction of the surface. Single surface operations are not as computationally difficult as working with Breps. Therefore it is advisable to try and work with single surfaces for as long as possible. 

### 📑4.2.3 Polysurfaces

If you create two surfaces with edges that align, you can join them together to create a polysurface. The egdes of a surface that are not connected to anything are called open or naked edges. The edges that are connected are called interior edges. If a surface does not have any open edges, it is considered a closed shape and we call it a Brep. You can use the [**ShowEdges**] command in Rhino or the [**BrepEdges**] component in Grasshopper to visualize the edges of any polysurface. 

![4.2.3a_SurfaceEdges.png](4.2.3a_SurfaceEdges.png)

Since a polysurface consists of a collection of joined surfaces, it is no longer possible to use control points to manipulate the UV-grid. You can use the [**Explode**] command in Rhino to separate the polysurface back into single surfaces that you can manipulate. 

## 📺 4.3 Designing with surfaces and patterns (50 min)

<div class="card">
    <p><strong>📌 What:</strong> Creating a surface using curves and creating a surface pattern (tutorials)</p>
    <p><strong>🙋 For Whom:</strong> Beginners in Rhino/Grasshopper</p>
    <p><strong>⌛ Time:</strong> 50 minutes</p>
</div>

### 📺Video Tutorial (30 min): 

<div class="tutorial-container">
    <div class="tip-details">
        <summary class="tutorial-summary">
            <strong>📺 Tutorial</strong>
        </summary>
        <div class="tip-div">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/8Rl-SYeyvSM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <div class="tutorial-container">
                <details class="tip-details">
                    <summary class="tutorial-summary">
                        <strong> Download the starting file here</strong>
                    </summary>
                    <div class="tip-div">
                        <p class="tip-p">

[Lesson 4 - Soapbottle starting file.3dm](Lesson_4_-_Soapbottle_starting_file.3dm)
                        </p>
                    </div>
                </details>
            </div>
        </div>
    </div>
</div>

---

### 📺Video Tutorial (20 min): surface pattern

<div class="tutorial-container">
    <div class="tip-details">
        <summary class="tutorial-summary">
            <strong>📺 Tutorial</strong>
        </summary>
        <div class="tip-div">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/N-ACXptKQMA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>

## 📺 4.4 Creating an arm brace (35 min)

<div class="card">
    <p><strong>📌 What:</strong> Creating a surface based on a scan (tutorials)</p>
    <p><strong>🙋 For Whom:</strong> Intermediate in Rhino/Grasshopper</p>
    <p><strong>⌛ Time:</strong> 30 minutes</p>
</div>

### 📺Video Tutorial (35 min): building a brace

<div class="tutorial-container">
    <div class="tip-details">
        <summary class="tutorial-summary">
            <strong>📺 Tutorial</strong>
        </summary>
        <div class="tip-div">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/53fszj3rYBs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <div class="tutorial-container">
                <details class="tip-details">
                    <summary class="tutorial-summary">
                        <strong> Scan file and loft script downloads</strong>
                    </summary>
                    <div class="tip-div">
                        <p class="tip-p">

[Link to scan file](https://www.artec3d.com/3d-models/arm)<br>
[Lesson 4 - Loft troubleshooting.gh](Lesson_4_-_Loft_troubleshooting.gh)
                        </p>
                    </div>
                </details>
            </div>
        </div>
    </div>
</div>

---

## 💻 4.5 Assignment lesson 4 (max 2 hours)

<div class="assignment-container">
    <div class="tip-details">
        <summary class="assignment-summary">
            <strong>💻 Assignment </strong>
        </summary>
        <div class="tip-div">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/Lesson_4/4.5a_Assignment4.png?ref_type=heads" alt="Gitlab" class="tip-img">
            <div class="card">
                <p>💻 Model a product with a pattern on part of it. It can be a your own design or you can use the starting file to model the bike handle shown in the assignment. The pattern can be any shape you want.<br></p>
            </div>
            <div class="assignment-container">
                <details class="tip-details">
                    <summary class="assignment-summary">
                        <strong>Starting files</strong>
                    </summary>
                    <div class="tip-div">
                        <p class="tip-p">

[Assignment 4 - Bike handle Starting file.3dm](Assignment_4_-_Bike_handle_Starting_file.3dm)
                        </p>
                    </div>
                </details>
            </div>
        </div>
    </div>
</div>

<div class="tip-details">
    <summary class="showcase-summary">
        <strong>🎓Student Showcase:</strong> Check out the student showcase to see what other students made for this assignment
    </summary>
    <div class="tip-div">
    <details class="showcase-details" open>
        <summary class="showcase-summary">
            <strong>🎓2022</strong>
        </summary>
        <div class="gallery">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_4_/1.PNG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_4_/2.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_4_/3.PNG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_4_/4.jpg">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_4_/5.jpeg">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_4_/6.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_4_/7.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_4_/8.jpg">
        </div>
        <p style="text-align: center; color: #888; font-style: italic; padding-top: 20px;">With permission from students year 2022</p>
    </details>
    <details class="showcase-details" open>
        <summary class="showcase-summary">
            <strong>🎓2021</strong>
        </summary>
        <div class="gallery">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_4_/1.png">
        </div>
        <p style="text-align: center; color: #888; font-style: italic; padding-top: 20px;">With permission from students year 2021</p>
    </details>
    <details class="showcase-details" open>
        <summary class="showcase-summary">
            <strong>🎓2020</strong>
        </summary>
        <div class="gallery">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_4_/1.PNG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_4_/2.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_4_/3.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_4_/4.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_4_/5.jpg">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_4_/6.png">
        </div>
        <p style="text-align: center; color: #888; font-style: italic; padding-top: 20px;">With permission from students year 2020</p>
    </details>
    <details class="showcase-details" open>
        <summary class="showcase-summary">
            <strong>🎓2019</strong>
        </summary>
        <div class="gallery">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_4_/1.JPG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_4_/2.JPG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_4_/3.JPG">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_4_/4.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_4_/5.png">
<img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_4_/6.PNG">
        </div>
        <p style="text-align: center; color: #888; font-style: italic; padding-top: 20px;">With permission from students year 2019</p>
    </details></div>
</div>