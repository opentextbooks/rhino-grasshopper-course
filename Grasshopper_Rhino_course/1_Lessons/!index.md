# Lessons

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: 1_Lesson_1_-_Basics/!index
:link-type: doc
:img-top: 1_Lesson_1_-_Basics/cover_crop.jpg
:class-header: bg-light

Lesson 1 - Basics

:::
:::{grid-item-card}
:link: 2_Lesson_2_-_Curves_and_lists/!index
:link-type: doc
:img-top: 2_Lesson_2_-_Curves_and_lists/cover_crop.jpg
:class-header: bg-light

Lesson 2 - Curves and lists

:::
:::{grid-item-card}
:link: 3_Lesson_3_-_Data_structures/!index
:link-type: doc
:img-top: 3_Lesson_3_-_Data_structures/cover_crop.jpg
:class-header: bg-light

Lesson 3 - Data structures


:::
:::{grid-item-card}
:link: 4_Lesson_4_-_Surfaces/!index
:link-type: doc
:img-top: 4_Lesson_4_-_Surfaces/cover_crop.jpg
:class-header: bg-light

Lesson 4 - Surfaces


:::
:::{grid-item-card}
:link: 5_Lesson_5_-_Meshes/!index
:link-type: doc
:img-top: 5_Lesson_5_-_Meshes/cover_crop.png
:class-header: bg-light

Lesson 5 - Meshes


:::
