# Computational Design for (Industrial) Designers using Rhino Grasshopper

## Quick intro on book content
<iframe width="560" height="315" src="https://www.youtube.com/embed/RP5PBbT4B1k" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## About this book
In the past decade, computational design has made a substantial impact in various technical domains, such as architecture, civil engineering, but also Industrial Design Engineering. Subsequently, is has become an significant topic in the research agenda of Industrial Design Engineering (IDE, TU Delft), and taught in courses throughout our bachelor and master program, such as the Minor Advanced Prototyping (BSc level), and 'Computational Design for Digital Fabrication' (MSc level), as well as being applied by numerous students in their (graduation) projects. In relation to these educational contexts, we observed a need for the following: 1. a more individualized and self-paced learning experience, 2. a wish for an autonomous learning experience (i.e. outside a course), and 3. more advanced educational materials, specifically with bachelor students, seeking to deepen their knowledge. To meet these needs we have developed this open, interactive textbook, consisting of the following parts: 1. a practical, easy-to-get-started set of introductory lessons to Rhino Grasshopper 2. A knowledge base offering more advanced application of Rhino Grasshopper, and 3. showcases of student (graduation) projects to inspire and learn from. 

The introductory lessons are for the most part based on the course material taught in the Minor Advanced Prototyping, created and taught throughout the last 5 years. Concequently, this content has been applied by students with diverse (technical) backgrounds (i.e. students with majors in industrial design engineering, mechanical engineering, computer science, and electrical engineering, etc.). The items in the knowledge base follow from various research and/or educational activities of IDE, and are for a significant based on the course materials of the Computational Design elective. In the first edition of this book, the knowledge base contains a limited set of items, which we aim to extend the coming years. 

### For whom?
The book is primarily aimed at bachelor and master students with a technical background. The introductory lessons do not assume any specific prior skills or knowledge (in general or with Grasshopper) to get started. Yet, (some) experience with computer-aided design (CAD), programming, data processing, and/or mathematics will likely be helpful to really delve into the more complex topics covered in the knowledge base. 

#### Use in the context of a course
This book is part of the course materials in the following courses, taught at Industrial Design Engineering, Delft University of Technology:
-	Prototyping with/for digital fabrication (IO3850), part of the [Minor Advanced Prototyping](https://www.tudelft.nl/io/studeren/minors/advanced-prototyping) ([studyguide info](https://www.studiegids.tudelft.nl/a101_displayProgram.do?program_tree_id=30640))
-	Computational Design for Digital Fabrication, elective of Industrial Design Engineering, TU Delft ([studyguide info](https://www.studiegids.tudelft.nl/a101_displayCourse.do?course_id=66323))

If you use this course material as part of a Delft University of Technology course, the course might require that the homework assignments are handed in. Details on the procedure and location for submission will be provided in the course. Please consult the online learning environment or teacher of the course for further information and support.

#### Autonomous learning
The course materials are designed with the intent that you can also follow them by yourself. Note that no formal support is provided with these course materials. We unfortunately do not have the (educational) capacity to help you out with your personal design questions. For question regarding your personal design, please consult our [troubleshooting page](/Grasshopper_Rhino_course/4_Troubleshooting/!index.md), [(other) grasshopper online resources](/Grasshopper_Rhino_course/3_Inspiration_&_Further_Reading/3.2_External_Links/!index.md), forums, or a Grasshopper expert. If you have feedback or questions regarding these course materials, feel free to contact us. 

## Using Rhino Grasshopper
Throughout the book, we make use of commercial computer-aided-design (CAD) software, namely [Rhinoceros®](https://www.rhino3d.com), and specifically the (built-in) module called Grasshopper®, hereafter referred to as ‘Rhino’ and ‘Grasshopper’. Grasshopper (and all its available plugins) is a diverse and powerful tool, which combines two digital domains: offering the possibility to parametrically model something in 3D (the Rhino part), and the ability to compute things (the Grasshopper part) (e.g. vary input parameters, do all kinds of mathematical operations on them). Another great advantage of Grasshopper is does it uses visual scripting (combining and interlinking computational blocks in a visual way), which means you do not need prior experience with ‘conventional’ coding (i.e. writing lines of code).

Although we are aware of the limiting factors of relying on commercial software, we believe it (currently) offers the best option in terms of usability, functionality, robustness, and (extensive) use base, despite its closedness and (financial) barriers that might exist to be able to use such commercial software. 

## Computational design

### Computational design in the industrial design domain
This interactive textbook provides an educational resource into computational design for (industrial) designers, where lessons and examples in this textbook center around the application of computational design in relation to (human-centered) product design. This, in contrast to the architectural/build environment scale – a domain which also extensively utilizes computational design principles, and for which many (more) resources are already available. We see diverse opportunities for applying computational design tools within the industrial design domain, having potential to play a role in different ways within the design process. 

Firstly, we see an application of computational design, in relation to topics such as physical ergonomics, and (industrial) fabrication - topics which have long been part of industrial design engineering discourse. With technological developments in 3D scanning, digital fabrication, and computation in general throughout the past decade, we witness a shift from industrially fabricated series/mass produced products (configurable to an individual through sizing or adjustment), to the (automatic) generation of unique designs which fit every person individually. The linking pin between a 3D model (and other data) of an individual, unique human, and the means to fabricate unique, one-off designs, is computational design. The goal being of course to have even better fitting products, which have numerous advantages such as improved performance, higher levels of perceived comfort (e.g. also offering better medical outcomes of prostheses/orthoses usage).

Secondly, we see potential for computational design in design, for its generative capabilities. As designers take inspiration from nature (e.g. biomimicry) or work with living organisms (i.e. bio-design) to design novel product (systems), computational design approaches can be employed for instance to computationally simulate the ‘growing’ of bio-inspired or living designs. 

Furthermore, computational design tools might play a role in any design process, as creative tool. Here, it can enable designers in accelerating design explorations, e.g. to quickly create multiple design variants. In this application, we also assume artificial intelligence (AI) is very likely to accelerate this further in the near future. 

In conclusion, the lessons and knowledge base offered in this book focus on topics that are specifically relevant for and/or attuned to (industrial) design (scale), which can be categorized in relation to its goal (e.g. design for personalized fit/comfort/aesthetics), by its means (e.g. design for digital fabrication), or for its role in the design process (e.g. for design exploration). 

## Getting started
To be able to fully make use of the book, you will need to install (and have a license for) Rhinoceros (which includes Grasshopper natively from Rhino 6 and up). We kindly refer to your institutions website (if applicable) and/or the website of [Robert McNeel & Associates (creators of Rhino)](https://www.rhino3d.com) for details on system requirements, installation and licensing. 
The course materials in this book were created using Rhino 7, Grasshopper build 1.0.0007. If you experience any issues using the course materials with subsequent versions of Rhino, please let us know. We aim to keep the course materials up to date. 

## Book structure 
The introductory lessons are built up with the following components:

:::{card}
📌 Outlines: A short description of what you can expect in the upcoming section.

📑 Explanation text: Written explanations with supporting images.

👩‍🏫 Explanation videos: Explaining the course material in short lecture videos.

📺 Tutorial videos: Follow-along tutorials.

💡 Tips: Tips and tricks to make working in Rhino/Grasshopper easier.

🖱️ Exercises: Small practice questions. The solution is provided.

💻 Assignments: Open-ended assignments, to practice further with the course materials.
:::

## Colofon

::: {card}
**Editor:** [Willemijn Elkhuizen](https://www.tudelft.nl/io/over-io/personen/elkhuizen-ws)

**Authors:** [Anne van den Dool](https://www.linkedin.com/in/anne-van-den-dool-3123a790/), [Jose Martinez Castro](https://josemartinezcastro.com), [Wolf Song](https://www.tudelft.nl/io/over-io/personen/song-y), [Mehmet Ozdemir](https://www.tudelft.nl/io/over-io/personen/ozdemir-m), [Zjenja Doubrovski](https://www.tudelft.nl/io/over-io/personen/doubrovski-el), and [Toon Huysmans](https://www.tudelft.nl/io/over-io/personen/huysmans-t)

**Advisory team:** [Sander Minnoye](https://www.linkedin.com/in/sander-minnoye-6678086) and [Wolf Song](https://www.tudelft.nl/io/over-io/personen/song-y)

**Feedback/errors/bugs:** Please email to prototyping@tudelft.nl

**Please reference this book as follows:** [section authors]. (2024). [section heading]. In W.S. Elkhuizen (Ed.), Computational Design for (Industrial) Designers, using Rhino Grasshopper. Delft University of Technology. [https://doi.org/10.59490/tb.87](https://doi.org/10.59490/tb.87)

**Acknowledgements:** We would like to acknowledge Wybren ten Cate -alumnus of Industrial Design Engineering at TU Delft - who created and taught a first versions of the introductory lessons in 2019/2020 and 2021/2022, as part of the Minor Advanced Prototyping. We are also greatly indebted to our teaching assistants Niels van Silfhout and Dieter van Dortmont, who supported us at various times throughout the creation process, helping out with creating this open, interactive textbook. Thanks also to the open interactive textbook project team from the TU Delft, specifically Thom Groen, Carola van der Muren, and Huib Baetsen, for helping us out with all the technicalities of publishing an open interactive textbook. Moreover, we are grateful to Jamie Luik for his contribution of the cover page image (work shown follows from a project with the Amsterdam University Medical Center, carried out as part of the Minor Advanced Prototyping). And finally, thanks to the Michiel de Jong, Marcell Varkonyi, and the TU Delft Teaching and Learning Services, for bringing together likeminded educational professionals, to share insights, and offer inspiration to bring education (even) further. This book was made possible with funding received from the ‘Open Education Stimulation Fund’ by the Delft University of Technology.
:::

## License (incl. list of license exemptions)
This book is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/). Note that the following images have a different CC license or are excepted from the CC license terms:

Different (and possibly more restrictive) Creative Commons license:
- {ref}`Key hand measurements <fig_key_hand_measurements>`, 2021, by Y. Yang et al., under [CC-BY-NC-ND license](https://creativecommons.org/licenses/by-nc-nd/4.0/)
- 2 images under {ref}`Further explorations into customizing G-code <Fig_further_explorations>` (Center image: [Square Vase, Cup, and Bracelet Generator](https://www.thingiverse.com/thing:1216450), 2013, by Eckerput, under [CC-BY-SA 3.0 license](https://creativecommons.org/licenses/by-sa/3.0/). Right image: [Quadrifolium 3D Print](https://openverse.org/image/7ba58ce5-f98c-497b-9ff6-8f9a5d487db8?q=3d%20print), 2012, by fdecomite, under [CC BY 2.0 license](https://creativecommons.org/licenses/by/2.0/))

Copyright retained, exempted from Creative Commons license:
- {ref}`3D hand scanner, © 2015 by P. Smakman <3D_scanning_hand_smakman>`

This book is part of the [Interactive Open Textbooks](https://textbooks.open.tudelft.nl/textbooks/catalog/category/interactive) collection, and part of [TU Delft Open](https://textbooks.open.tudelft.nl/textbooks/index)
 
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
