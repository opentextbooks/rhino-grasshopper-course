import os
import re

def generate_year_container(base_url):
    # Extract the year from the base URL using regular expressions
    match = re.search(r'(\d{4})_', base_url)
    if match:
        year = match.group(1)
    else:
        print("Year not found in the URL")
        return None

    # Construct the correct year string
    permission_text = f"With permission from students year {year}"

    # Extract the last two directories from the base URL
    base_url_parts = base_url.split("/")
    image_directory = "/".join(base_url_parts[-2:])

    # Generate Markdown for each image in the directory
    markdown_images = []
    for filename in os.listdir(image_directory):
        # Skip files that don't end with .png, .jpeg, or .gif
        if not filename.lower().endswith(('.png', '.jpeg', '.jpg', '.gif')):
            continue
        # Construct the image URL
        image_url = f"{base_url}/{filename}"
        # Create Markdown for the image
        markdown_image = f'<img src="{image_url}">'
        markdown_images.append(markdown_image)

    # Combine all Markdown images into a single string
    markdown_output = '\n'.join(markdown_images)

    # Generate the complete container structure
    container_structure = f'''
    <details class="showcase-details" open>
        <summary class="showcase-summary">
            <strong>🎓{year}</strong>
        </summary>
        <div class="gallery">
            {markdown_output}
        </div>
        <p style="text-align: center; color: #888; font-style: italic; padding-top: 20px;">{permission_text}</p>
    </details>'''

    return container_structure

# Base URLs for different years
base_urls = [
    "https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2022_1_",
    "https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2021_1_",
    "https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2020_1_",
    "https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_",
    # Add more base URLs for additional years if needed
]

# Generate container structures for each year
container_structures = ''
for base_url in base_urls:
    year_container = generate_year_container(base_url)
    if year_container:
        # Concatenate year containers without additional line breaks
        container_structures += year_container

# Generate the complete HTML
html_output = f'''
<div class="tip-details">
    <summary class="showcase-summary">
        <strong>🎓Student Showcase:</strong> Check out some of the examples below to see what students created for this assignment in previous years
    </summary>
    <div class="tip-div">{container_structures}</div>
</div>'''

# Print the final HTML
print(html_output)
