<div style="color: #5c836b; margin: 10px 0;">
    <details style="background-color: #FFFFFF; border-radius: 20px; overflow: hidden; box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.1);">
        <summary style="padding: 10px; background-color: #E5F9ED; border-radius: 0px">
            <strong>💡Tip:</strong> A faster way to create your sliders
        </summary>
        <div style="color: #000000; padding-top: 20px; padding-bottom:20px; padding-left:20px; padding-right: 20px">
            <p style="font-size: calc(1em - 2px);">
                For a faster way to create sliders, you can type a number in the search bar to create a slider. You can also create a slider with a set range by typing the minimum followed by two dots (..) and the maximum number. The amount of digits you use to type the numbers will determine the accuracy of the slider.        
            </p>
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/1.2.2i_SliderTip.gif" alt="Gitlab">
        </div>
    </details>
</div>


<div style="color: #89833f; margin: 10px 0;">
    <details style="background-color: #FFFFFF; border-radius: 20px; overflow: hidden; box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.1);">
        <summary style="padding: 10px; background-color: #fffabe; border-radius: px">
            <strong>💡Tip:</strong> A faster way to create your sliders
        </summary>
        <div style="color: #000000; padding-top: 15px; padding-bottom:20px; padding-left:20px; padding-right: 20px">
            <p style="font-size: calc(1em - 2px);">
                For a faster way to create sliders, you can type a number in the search bar to create a slider. You can also create a slider with a set range by typing the minimum followed by two dots (..) and the maximum number. The amount of digits you use to type the numbers will determine the accuracy of the slider.        
            </p>
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/1.2.2i_SliderTip.gif" alt="Gitlab" style="float: left; margin-bottom: 20px;">
        </div>
    </details>
</div>


<div class="tip-container">
    <details class="tip-details">
        <summary class="tip-summary">
            <strong>💡Tip:</strong> Switch between icons and text
        </summary>
        <div class="tip-div">
            <p class="tip-p">
                You can switch between icons and text components in the <strong>[Display]</strong> menu at the top of the interface. You can also switch on <strong>[Draw full names]</strong> to make components easier to understand.         
            </p>
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/1.2.2d_GrasshopperDisplaysettings.png" alt="Gitlab" class="tip-img">
        </div>
    </details>
</div>


Colours: 

Notes: #E5F7FF







<div class="tip-details">
    <summary class="showcase-summary">
        <strong>🎓Student Showcase:</strong> Check out the student showcase to see what other students made for this assignment
    </summary>
    <div class="tip-div">
        <div class="gallery">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/Android and apple Sylvia Ko 4663837 AP.PNG">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/Iza Bosch Assignment 1B-libelle rhino.JPG">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/Jason Smit Duck.png">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/Jeppe Piersma.png">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/Joram Boumans.png">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/Lisa Ye Cat Rhino.PNG">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/Walrus_Coen_Jansen.JPG">
            <img src="https://gitlab.tudelft.nl/opentextbooks/rhino-grasshopper-course/-/raw/Dieter_update_branch/Images_TRY/2019_1_/Yoshi Verspaget.PNG">
        </div>
        <p style="text-align: center; color: #888; font-style: italic; padding-top: 20px;">With permission from students year 2019</p>
    </div>
</div>

<div class="card">
    <p style="margin-bottom: 1em;"><strong>📌 Outlines:</strong> A short description of what you can expect in the upcoming section.</p>
    <p style="margin-bottom: 1em;"><strong>📑 Explanation text:</strong> Written explanations with supporting images.</p>
    <p style="margin-bottom: 1em;"><strong>👩‍🏫 Explanation videos:</strong> Explaining the course material in short lecture videos.</p>
    <p style="margin-bottom: 1em;"><strong>📺 Tutorial videos:</strong> Follow-along tutorials.</p>
    <p style="margin-bottom: 1em;"><strong>💡 Tips:</strong> Tips and tricks to make working in Rhino/Grasshopper easier.</p>
    <p style="margin-bottom: 1em;"><strong>🖱️ Exercises:</strong> Small practice questions. The solution is provided.</p>
    <p><strong>💻 Assignments:</strong> Open-ended assignments, to practice further with the course materials.</p>
</div>


:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Personalized_Fit/!index
:link-type: doc
:img-top: Personalized_Fit/cover_crop.jpg
:class-header: bg-light

Persononalized Fit
:::

:::{grid-item-card}
:link: Digital_Fabrication/!index
:link-type: doc
:img-top: Digital_Fabrication/Laser_Cutting_cover_crop.png
:class-header: bg-light

Digital Fabrication
:::

:::{grid-item-card}
:link: Generative_Design/!index
:link-type: doc
:img-top: Generative_Design/cover_crop.png
:class-header: bg-light

Generative Design
:::








# Knowledge Base

The knowledge base for the Rhino Grasshopper course serves as a comprehensive resource, providing detailed tutorials on applied topics in industrial design. As a dynamic platform, it's designed to grow, with plans to continually expand by incorporating more information, insights, and resources in the future to enhance the learning and exploration of computational design in the faculty.

## Personalized Fit

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3


:::{grid-item-card}
:link: Human_3D_Shape_Data_and_Models/!index
:link-type: doc
:img-top: Human_3D_Shape_Data_and_Models/cover_crop.png
:class-header: bg-light

Human 3D Shape Data and Models
:::

:::{grid-item-card}
:link: Data-Driven_Personalized_Design/!index
:link-type: doc
:img-top: Data-Driven_Personalized_Design/cover_crop.png
:class-header: bg-light

Data-Driven Personalized Design
:::


## Digital Fabrication

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3


:::{grid-item-card}
:link: Laser_Cutting/!index
:link-type: doc
:img-top: Laser_Cutting/cover_crop.png
:class-header: bg-light

Laser Cutting
:::

:::{grid-item-card}
:link: 3D_Printing/!index
:link-type: doc
:img-top: 3D_Printing/cover_crop.png
:class-header: bg-light

3D Printing
:::

## Generative Design

:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: Generative_Form_Finding/!index
:link-type: doc
:img-top: Generative_Form_Finding/cover_crop.png
:class-header: bg-light

Generative Form Finding
:::









:::::{grid} 1 1 2 3
:class-container: text-center
:gutter: 3

:::{grid-item-card}
:link: 3.1_Graduation_Projects/!index
:link-type: doc
:img-top: Graduation_cover_crop.png
:class-header: bg-light

Graduation Projects
:::







